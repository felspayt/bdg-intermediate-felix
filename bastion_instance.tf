# Creating bastion instance for accessing hosts in private subnets.

resource "aws_instance" "bastion" {
  ami           = var.bastion_ami
  instance_type = "t2.micro"
  key_name      = "yourkey"             #Enter your .pem key name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  subnet_id  = aws_subnet.alb_public2.id
  availability_zone = "eu-west-1b"
  
tags = {
    Name = "bastion"
  }

     connection {
            //Use public IP of the instance to connect to it.
            host          = "${aws_instance.ins1_ec2.public_ip}"
            type          = "ssh"
            user          = "${file("yourkey.pem")}" #Enter your .pem key here
            private_key   = "${file("ney-key-tf.pem")}"
            timeout       = "1m"
            agent         = false
        }
}

