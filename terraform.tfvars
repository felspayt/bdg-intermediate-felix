# Defining values for the variables.

ami          = "ami-00ae935ce6c2aa534"
bastion_ami  = "ami-00ae935ce6c2aa534"
aws_region   = "eu-west-1"
cidr_vpc     = "10.0.0.0/24"
cidr_public1 = "10.0.0.0/28"
cidr_public2 = "10.0.0.16/28"
cidr_web     = "10.0.0.32/28"
cidr_db      = "10.0.0.48/28"
