#Creating Application load balancer, target group and listener on port 80.

resource "aws_lb" "my_lb" {
  name               = "my-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_subnet.alb_public1.id, aws_subnet.alb_public2.id]

  tags = {
    name = "my_lb"
  }

}

resource "aws_lb_target_group" "my_target_group" {
  name              = "my-target-group"
  port              = 80
  protocol          = "HTTP"
  vpc_id            = aws_vpc.my_vpc.id

health_check {
   interval            = 70
   path                = "/index.html"
   port                = 80
   healthy_threshold   = 2
   unhealthy_threshold = 2
   timeout             = 60 
   protocol            = "HTTP"
   matcher             = "200,202"

 }
}

resource "aws_lb_listener" "my_lb_listener" {
  load_balancer_arn = aws_lb.my_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group.arn

  }
}
