#Create Launch config

resource "aws_launch_configuration" "webserver-launch-config" {
  name_prefix     = "webserver-launch-config"
  image_id        =  var.ami
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web_sg.id]
  key_name        = "yourkey"             #Enter your .pem key name

  lifecycle {
        create_before_destroy = true
     }

  connection {
        type          = "ssh"
        user          = "ec2-user"
        private_key   = "${file("yourkey.pem")}" #Enter your .pem key here
        timeout       = "1m"
        agent         = false
  }

  user_data = filebase64("apache_server.sh")
}