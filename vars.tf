# Declaring variables.
variable "ami"{
  type        = string
}

variable "bastion_ami"{
  type        = string
}

variable "aws_region" {
  type        = string
  description = "default region"
}

variable "cidr_vpc" {
  type        = string
  description = "Declaring variable for the vpc_cidr_block"
}

variable "cidr_public1" {
  type = string
  description = "Declaring variable for the cidr of the public1 subnet"
}

variable "cidr_public2" {
  type        = string
  description = "Declaring variable for the cidr of the public2 subnet"
}

variable "cidr_web" {
  type = string
  description = "Declaring variable for the cidr of the web private subnet"
}

variable "cidr_db" {
  type = string
  description = "Declaring variable for the cidr of the db private subnet"
}
