#creating AutoScaling Group

resource "aws_autoscaling_group" "web_asg" {
  name              = "web_asg"
   desired_capacity    = 1
  max_size             = 2
  min_size             = 1
  health_check_grace_period = 50
  health_check_type = "EC2"
  wait_for_capacity_timeout = "7m"
  termination_policies = ["OldestInstance"]
  launch_configuration = aws_launch_configuration.webserver-launch-config.id
  vpc_zone_identifier  = [aws_subnet.web_private.id, aws_subnet.db_private.id]
  target_group_arns    = [aws_lb_target_group.my_target_group.arn]

  tag {
    key                 = "Name"
    value               = "web_server"
    propagate_at_launch = true
  }
}
